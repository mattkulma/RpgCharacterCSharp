﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgCharacterCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            Builder person = new Builder();

            Console.WriteLine("Welcome to the Character Creator");
            Console.WriteLine("Would you like to Make a new Character? \n"
                + "yes or no?"
                );
            string input = Console.ReadLine();

            while (input != "no")
            {
                if (input.ToLower() == "yes")
                {
                    person.MakeCharacter();
                }
                else
                {
                    Console.WriteLine("Maybe next time, Good Bye");
                    Console.WriteLine("Press enter to leave");
                    Console.ReadLine();

                }

                Console.WriteLine("Would you Like to make another character? \n"
                    + "yes or no?"
                    );

                input = Console.ReadLine();


            }
        }
    }
}
