﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace RpgCharacterCSharp
{
    class Builder
    {
        public Builder() { }

        Character myCharacter = new Character();


        public void MakeCharacter()
        {
            Console.WriteLine("What is the name of your character?");
            string name = Console.ReadLine();
            Console.WriteLine("What race is your character?");
            string race = Console.ReadLine();
            Console.WriteLine("Tell me about " + name + ".");
            string narative = Console.ReadLine();
            Console.WriteLine("\n");

            Random random = new Random();
            myCharacter.Strength = random.Next(2, 19);
            myCharacter.Intelligence = random.Next(2, 19);
            myCharacter.Wisdom = random.Next(2, 19);
            myCharacter.Dexterity = random.Next(2, 19);
            myCharacter.Constitution = random.Next(2, 19);
            myCharacter.Charisma = random.Next(2, 19);


            Console.WriteLine("Here is your Character!");
            Console.WriteLine("Your characters name is: " + name + "\n");
            Console.WriteLine(name + " is a/an: " + race + "\n");
            Console.WriteLine("Here are some things about" + name + ": \n" + narative + "\n");
            Console.WriteLine("Their Strength is: " + myCharacter.Strength);
            Console.WriteLine("Their Intelligence is: " + myCharacter.Intelligence);
            Console.WriteLine("Their Wisdom is: " + myCharacter.Wisdom);
            Console.WriteLine("Their Dexterity is: " + myCharacter.Dexterity);
            Console.WriteLine("Their Constitution is: " + myCharacter.Constitution);
            Console.WriteLine("Their Charisma is: " + myCharacter.Charisma + "\n");


            Console.WriteLine("Would you like to save him to a file? (yes of no)");
            string input = Console.ReadLine();
            try
            {
                if (input.ToLower() == "yes")
                {

                    //string path = @"c:/temp/" + name + ".txt";
                    string folder = Environment.CurrentDirectory;
                    string file = name + ".txt";
                    string path = Path.Combine(folder, file);

                    if (!File.Exists(path))
                    {
                        using (StreamWriter writer = File.CreateText(path))
                        {
                            writer.WriteLine(name + "\n");
                            writer.WriteLine(race + "\n");
                            writer.WriteLine(narative + "\n");
                            writer.WriteLine("Strength: " + myCharacter.Strength);
                            writer.WriteLine("Intelligence: " + myCharacter.Intelligence);
                            writer.WriteLine("Wisdom: " + myCharacter.Wisdom);
                            writer.WriteLine("Dexterity: " + myCharacter.Dexterity);
                            writer.WriteLine("Constitution: " + myCharacter.Constitution);
                            writer.WriteLine("Charisma: " + myCharacter.Charisma);
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                throw;
            }



        }


    }
}

