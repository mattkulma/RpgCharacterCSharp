﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpgCharacterCSharp
{
    public class Character
    {
        //public Character(string name, string narative, string race, int strength, int intelegence, int wisdom, int dextartiy, int constitution, int charisma)
        //{
        //    this.Name = name;
        //    this.Race = race;
        //    this.Narative = narative;
        //    this.Strength = strength;
        //    this.Intelegence = intelegence;
        //    this.Wisdom = wisdom;
        //    this.Dextarity = dextartiy;
        //    this.Constitution = constitution;
        //    this.Charisma = charisma;
        //}

        public Character()
        {
        }

        public string Name { get; set; }
        public string Narative { get; set; }
        public string Race { get; set; }
        public int Strength { get; set; }
        public int Intelligence { get; set; }
        public int Wisdom { get; set; }
        public int Dexterity { get; set; }
        public int Constitution { get; set; }
        public int Charisma { get; set; }


    }
}
